//
//  ScanBarCodeVCViewController.swift
//  Brazen
//
//  Created by Sanjeev Mehta on 2/13/20.
//  Copyright © 2020 sanjeevMehta. All rights reserved.
//

import UIKit
import AVFoundation

class ScanQRCodeVC: UIViewController , AVCaptureMetadataOutputObjectsDelegate {
    
        var captureSession: AVCaptureSession!
        var previewLayer: AVCaptureVideoPreviewLayer!
        var joinerID = String()
        var token = String()
    
        override func viewDidLoad() {
            super.viewDidLoad()
            
            view.backgroundColor = UIColor.black
            captureSession = AVCaptureSession()
            
            guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
            let videoInput: AVCaptureDeviceInput
            
            do {
                videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
            } catch {
                return
            }
            
            if (captureSession.canAddInput(videoInput)) {
                captureSession.addInput(videoInput)
            } else {
                failed()
                return
            }
            
            let metadataOutput = AVCaptureMetadataOutput()
            
            if (captureSession.canAddOutput(metadataOutput)) {
                captureSession.addOutput(metadataOutput)
                
                metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                metadataOutput.metadataObjectTypes = [.qr]
            } else {
                failed()
                return
            }
            
            previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            previewLayer.frame = view.layer.bounds
            previewLayer.videoGravity = .resizeAspectFill
            view.layer.addSublayer(previewLayer)
            
            captureSession.startRunning()
        }
        
        func failed() {
            let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
            captureSession = nil
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            
            if (captureSession?.isRunning == false) {
                captureSession.startRunning()
            }
              AppUtility.lockOrientation(.portrait)
           
            
            self.navigationController?.navigationBar.tintColor = UIColor.white

        }
        
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            
            if (captureSession?.isRunning == true) {
                captureSession.stopRunning()
            }
        }
        
        func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
            captureSession.stopRunning()
            
            if let metadataObject = metadataObjects.first {
                guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
                guard let stringValue = readableObject.stringValue else { return }
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
               
              
           
                if let range = stringValue.range(of: "token=") {
                    let firstPart = stringValue[stringValue.startIndex..<range.lowerBound]
                    print(firstPart)
                    self.joinerID = "\(firstPart)"
                    // print Hello
                }
                if let range = stringValue.range(of: "=") {
                    let phone = stringValue[range.upperBound...]
                    print(phone)
                    self.token = "\(phone)"
                    // prints "123.456.7891"
                }
                  found()
                
            }
            
           // dismiss(animated: true)
        }
        
        func found() {
            self.performSegue(withIdentifier: "showCall", sender: self)
        }
        
        override var prefersStatusBarHidden: Bool {
            return true
        }
        
//        override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//            return .portrait
//
//        }
//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! VideoCallVC
        destinationVC.token = self.token
        destinationVC.joinerid = self.joinerID
        let userdefaults = UserDefaults.standard
        userdefaults.setValue(token, forKey: "token")
        userdefaults.setValue(joinerID, forKey: "joinerID")
         VideoCallVC.defaultChannelName = joinerID
        UserDefaults.standard.set("Started", forKey: "Started")

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.view.removeFromSuperview()
        AppUtility.lockOrientation(.all)

    }

}
