//
//  WelcomeSecondVC.swift
//  Brazen
//
//  Created by Sanjeev Mehta on 2/14/20.
//  Copyright © 2020 sanjeevMehta. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class WelcomeSecondVC: UIViewController {

    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var videoView: VideosView!
     let videoArray = ["output","introVideo1","intoVideo2Short"]
    var videoTag = 0
    override func viewDidLoad() {
        super.viewDidLoad()

      playVideo(tag:videoTag)
        startBtn.dropShadow(cornerRadius: true)

    }
    func playVideo(tag:Int){
        videoView.configure(forResource: videoArray[tag], gravity: .resizeAspectFill)
        videoView.play()
        NotificationCenter.default.addObserver(self, selector: #selector(reachTheEndOfTheVideo(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.videoView.player?.currentItem)
        
    }
    
    @objc func reachTheEndOfTheVideo(_ notification: Notification){
        if videoTag != 2{
            videoTag += 1
             playVideo(tag: videoTag)
        }else{
            videoTag = 0
            playVideo(tag: videoTag)
        }
        
    }

    @IBAction func startBtnActn(_ sender: Any) {
        checkInternet()
        self.performSegue(withIdentifier: "showScanVC", sender: self)        
    }
    func checkInternet(){
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
            let alert = UIAlertController(title: "Alert", message: "Something went wrong. please check your internet connection.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                //KRProgressHUD.dismiss()
                // self.performSegue(withIdentifier: "showHome", sender: self)
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
    }

}
