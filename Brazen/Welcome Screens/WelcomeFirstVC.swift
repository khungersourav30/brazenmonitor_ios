//
//  WelcomeFirstVC.swift
//  Brazen
//
//  Created by Sanjeev Mehta on 2/14/20.
//  Copyright © 2020 sanjeevMehta. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class WelcomeFirstVC: UIViewController {

    var isLoop: Bool = false

    @IBOutlet weak var videoView: VideosView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
       
        videoView.configure(forResource: "StartupBrazen copy", gravity: .resizeAspect)
        videoView.play()
         NotificationCenter.default.addObserver(self, selector: #selector(reachTheEndOfTheVideo(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.videoView.player?.currentItem)
    }
    
    @objc func reachTheEndOfTheVideo(_ notification: Notification){

            self.performSegue(withIdentifier: "showSecondVC", sender: self)

        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
    }
    
}
