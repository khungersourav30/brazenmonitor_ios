//
//  HomeVC.swift
//  Brazen
//
//  Created by Sanjeev Mehta on 2/14/20.
//  Copyright © 2020 sanjeevMehta. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var scanBtn: UIButton!
    @IBOutlet weak var connectBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
      

        scanBtn.dropShadow(cornerRadius: true)
        connectBtn.dropShadow(cornerRadius: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let clr = UIColor(red: 108/255, green: 225/255, blue: 183/255, alpha: 1)
        navigationController?.navigationBar.barTintColor = clr

        
      setTitleNav()
      AppUtility.lockOrientation(.portrait)
        
    }
    
    func setTitleNav(){
        let customView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 100, height: 44.0))
        customView.backgroundColor = UIColor.clear
        
        let label = UILabel(frame: CGRect(x: 10.0, y: 0.0, width: customView.frame.width, height: 44.0))
        label.font = UIFont(name: "Helvetica Neue", size: 24)
        label.text = "Brazen"
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.right

        customView.addSubview(label)
        
        let leftButton = UIBarButtonItem(customView: customView)
        self.navigationItem.leftBarButtonItem = leftButton
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = false
        AppUtility.lockOrientation(.all)
    }

    func checkInternet(){
        if !Reachability.isConnectedToNetwork(){
            let alert = UIAlertController(title: "Alert", message: "Something went wrong. please check your internet connection.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
             
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    @IBAction func connectBtnActn(_ sender: Any) {
        checkInternet()
        self.performSegue(withIdentifier: "showVideoCallVC", sender: self)
    }
    @IBAction func scanAgainBtnActn(_ sender: Any) {
        checkInternet()
        self.performSegue(withIdentifier: "showScanVC", sender: self)
    }
}
