//
//  extension+Maps.swift
//  Brazen
//
//  Created by Sanjeev Mehta on 2/14/20.
//  Copyright © 2020 sanjeevMehta. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

extension VideoCallVC: MKMapViewDelegate, CLLocationManagerDelegate{
    
    func setMapImageViewGesture(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapImage))
        expandMapViewImage.isUserInteractionEnabled = true
        expandMapViewImage.addGestureRecognizer(tap)
    }
    
    @objc func tapImage(){
    print("Tapped")
        isExpandMap = true
        timerView.isHidden = true
        expandMapView.isHidden = false
    }
    
    func setCurrentLocation(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func setMap(lat:Double,long:Double){
        for i in mapView.annotations{
            mapView.removeAnnotation(i)
        }
        for i in expandMap.annotations{
            expandMap.removeAnnotation(i)
        }
        for i in roundMapView.annotations{
            roundMapView.removeAnnotation(i)
        }
        let initialLocation = CLLocation(latitude: lat, longitude: long)
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegion(center: initialLocation.coordinate,latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
        expandMap.setRegion(coordinateRegion, animated: true)
        roundMapView.setRegion(coordinateRegion, animated: true)
        
        let annotation = MKPointAnnotation()
        let centerCoordinate = CLLocationCoordinate2D(latitude: lat, longitude:long)
        annotation.coordinate = centerCoordinate
        
        annotation.title = "Baby...."
        
        mapView.addAnnotation(annotation)
        expandMap.addAnnotation(annotation)
        roundMapView.addAnnotation(annotation)
        mapView.showAnnotations(mapView.annotations, animated: true) //this fixed it

    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Don't want to show a custom image if the annotation is the user's location.
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        // Better to make this class property
        let annotationIdentifier = "AnnotationIdentifier"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            let av = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            av.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            annotationView = av
        }
        
        if let annotationView = annotationView {
            // Configure your annotation view here
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "mapIcon")
        }
        
        return annotationView
    }
}


class CustomPointAnnotation: MKPointAnnotation {
    var imageName: String!
}
