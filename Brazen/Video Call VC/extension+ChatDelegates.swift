//
//  extension+ChatDelegates.swift
//  Brazen
//
//  Created by Sanjeev Mehta on 2/17/20.
//  Copyright © 2020 sanjeevMehta. All rights reserved.
//

import Foundation
import UIKit
//import TwilioChatClient
import KRProgressHUD
import TwilioVideo

/////Twilio Data Track Delegates Methods/////////
extension VideoCallVC: RemoteDataTrackDelegate{
    
    // Participant has published a data track.
    func remoteParticipantDidPublishDataTrack(participant: RemoteParticipant,publication: RemoteDataTrackPublication) {
        print(participant.identity)
    }
    
    // Participant has unpublished a data track.
    func remoteParticipantDidUnpublishDataTrack(participant: RemoteParticipant,publication: RemoteDataTrackPublication) {
        
    }
    
    // Data track has been subscribed to and messages can be observed.
    func didSubscribeToDataTrack(dataTrack: RemoteDataTrack,publication: RemoteDataTrackPublication,participant: RemoteParticipant) {
        // Respond to incoming messages.
        logMessage(messageText: "Subscribed to data track for Participant \(participant.identity)")

        dataTrack.delegate = self
        
    }
    
    // Data track has been unsubsubscribed from and messages cannot be observed.
    func didUnsubscribeFromDataTrack(dataTrack: RemoteDataTrack,publication: RemoteDataTrackPublication,participant: RemoteParticipant) {
    }


    func remoteDataTrackDidReceiveString(remoteDataTrack: RemoteDataTrack, message: String) {
        print(message)
        
        if let msg = convertToDictionary(text:message){
        setValueInLabels(msg: msg)
        }
        KRProgressHUD.dismiss()
    }
    
    func remoteDataTrackDidReceiveData(remoteDataTrack: RemoteDataTrack, message: Data) {
        print(message)
    }
    func setValueInLabels(msg:[String:Any]){
        let wifi = msg["wifiSignal"] as! String
        let networkSignal = msg["networkSignal"] as? String ?? "No Signal"
        let batteryLevel = msg["batteryLevel"] as! String
        let batteryTemp = msg["batteryTemp"] as! String
        let lat = msg["latitute"] as! String
        let long = msg["longitute"] as! String
        let chargingStatus = msg["chargingStatus"] as! String
        self.wifiLbl.text = wifi
        self.networkLbl.text = networkSignal
        self.batterylvlLbl.text = batteryLevel
        self.temperatureLbl.text = batteryTemp
        let batteryLvl = batteryLevel.replacingOccurrences(of: "%", with: "")
        print(batteryLvl)
        if chargingStatus == "0"{
            
            if Int(batteryLvl)! < 20 && Int(batteryLvl)! > 10{
                
                if self.alertCount == 0{
                    print("enter")
                    self.showAlert(message: "Device battery is Low")
                    self.alertCount += 1
                }
                
            }else if Int(batteryLvl)! < 10 {
                if self.alertCount != 2 || self.alertCount == 0 {
                    self.showAlert(message: "Device battery is Low")
                    self.alertCount += 1
                }
                if self.alertCount == 1{
                    self.showAlert(message: "Device battery is Low")
                    self.alertCount += 1
                }
            }
        }
        if timeTag == 0{
            self.callTimer()
            self.timeTag += 1
        }
        //if Int(batteryTemp)
        //KRProgressHUD.dismiss()
        self.hideIndicatorView()
        self.setMap(lat: Double(lat)!, long:Double(long)!)
    }

}
extension VideoCallVC : LocalParticipantDelegate {
    func localParticipantDidPublishDataTrack(participant: LocalParticipant, dataTrackPublication: LocalDataTrackPublication) {
        // The data track has been published and is ready for use
        print("abc")
        guard let localDataTrack = dataTrackPublication.localTrack else {
            return
        }
        
        
       
    }
}





/////////Twilio Chat Process/////////
// MARK: - TwilioChatClientDelegate
//extension VideoCallVC : TwilioChatClientDelegate {
//
//    func chatClient(_ client: TwilioChatClient, synchronizationStatusUpdated status: TCHClientSynchronizationStatus) {
//        if status == TCHClientSynchronizationStatus.completed {
//            UIApplication.shared.isNetworkActivityIndicatorVisible = false
//            self.channelsList = client.channelsList()
//            self.populateChannels()
//
//            print(status.rawValue)
//            self.joinGeneralChatRoomWithCompletion { (succeed) in
//                print(succeed)
//                self.generalChannel.getMembersCount(completion: { (result, count) in
//                    print(result,count)
//                })
//
//            }
//
//    }
//}
// }
//
//extension VideoCallVC:TCHChannelDelegate{
//
//    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, messageAdded message: TCHMessage) {
//       // print(message.body!)
//       let msg = convertToDictionary(text:message.body!)
//      //  print(msg)
//        if let msgs = msg{
//
//        setValueInLabels(msg: msgs)
//        }
//
//    }
//
//
//}

