//
//  VideoCallVC.swift
//  Brazen
//
//  Created by Sanjeev Mehta on 2/13/20.
//  Copyright © 2020 sanjeevMehta. All rights reserved.
//

import UIKit
import TwilioVideo
import MapKit
import CoreLocation
//import TwilioChatClient
import KRProgressHUD

class VideoCallVC: UIViewController {
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var mapVisualEffect: UIVisualEffectView!
    @IBOutlet weak var timerView: UIView!
    @IBOutlet weak var versionBtn: UIButton!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var wifiLbl: UILabel!
    @IBOutlet weak var networkLbl: UILabel!
    @IBOutlet weak var temperatureLbl: UILabel!
    @IBOutlet weak var batterylvlLbl: UILabel!
    @IBOutlet weak var videoCloseBtn: UIButton!
    @IBOutlet weak var roundMapView: MKMapView!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var muteUnmuteBtn: UIButton!
    @IBOutlet weak var expandVideoViewImage: UIImageView!
    @IBOutlet weak var expandVideoView: UIView!
    @IBOutlet weak var expandMapViewImage: UIImageView!
    @IBOutlet weak var expandMapView: UIView!
    @IBOutlet weak var expandMap: MKMapView!
    @IBOutlet weak var smallVideoView: UIView!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var connectDisconnectBtn: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    
    var accessToken = "TWILIO_ACCESS_TOKEN"
    
    var alertCount = 0
    var seconds = 0
    var minutes = 0
    var Hours = 0
    var timeTag = 0
    // Video SDK components
    var room: Room?
    var camera: CameraSource?
    var localVideoTrack: LocalVideoTrack?
    var localAudioTrack: LocalAudioTrack?
    var remoteParticipant: RemoteParticipant?
    var remoteView: VideoView?
    var deviceId = String()
    var joinerid = String()
    var token = String()
    var isExpandMap = false
    
    static let defaultChannelUniqueName = String()
    static var defaultChannelName = "General Channel"
    
  //  var channelsList:TCHChannels?
    var channels:NSMutableOrderedSet?
  //  var generalChannel:TCHChannel!
    
    var localDataTrack = LocalDataTrack()

    var locationManager = CLLocationManager()
    deinit {
        // We are done with camera
        if let camera = self.camera {
            camera.stopCapture()
            self.camera = nil
        }
    }
    @IBOutlet weak var previewView: VideoView!
    
   // var client:TwilioChatClient?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideIndicatorView()
          KRProgressHUD.showOn(self).show(withMessage: "Connecting...")
       deviceId = UIDevice.current.identifierForVendor!.uuidString
      
        HitApi(id: deviceId)
        self.previewView.isHidden = true
        
        mapView.delegate = self
        expandMap.delegate = self
        roundMapView.delegate = self
       
        setMapImageViewGesture()
        setExpandVideoImageViewGesture()
        roundView.layer.cornerRadius = roundView.frame.width/2
        roundMapView.layer.cornerRadius = roundMapView.frame.width/2
        // self.setMap()
        self.navigationController?.navigationBar.isHidden = true
        
        Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(self.setTimeForConnecting), userInfo: nil, repeats: false)
        versionBtn.setTitle("v \(Bundle.main.releaseVersionNumber!)", for: .normal)
    
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         AppUtility.lockOrientation(.portrait)
    }
    
    func showIndicatorView()  {
        mapVisualEffect.alpha = 0.6
        mapVisualEffect.isHidden = false
        activityIndicatorView.isHidden = false
        activityIndicatorView.startAnimating()
        
    }
    func hideIndicatorView()  {
       // mapVisualEffect.alpha = 0.6
        mapVisualEffect.isHidden = true
        activityIndicatorView.isHidden = true
        activityIndicatorView.stopAnimating()
        
    }
    
    func HitApi(id:String){
        if UserDefaults.standard.object(forKey: "joinerID") != nil{
           self.joinerid = UserDefaults.standard.object(forKey: "joinerID") as! String
            self.token = UserDefaults.standard.object(forKey: "token") as! String
           
        }else{
            
            return
        }
        let parm = ["userid": id,"joinerid":self.joinerid,"token":self.token]
        print(parm)
        VideoCallVC.defaultChannelName = self.joinerid
        
        WebService.shareInstance.getToken(view: self, parm: parm) { (json, error) in
            if error == nil{
               // print(json)
                
                let swiftyJson = json
                let b = swiftyJson["token"].stringValue
                print(b)
                
                //self.showIndicatorView()
                self.connectCall(accessToken: b)
//                self.connectClientWithCompletion(completion: { (bool, error) in
//                    print("callback")
//
//                })
                self.localAudioTrack?.isEnabled = false
            }else{
                print(error)
            }
        }
    }
    func callTimer(){
        DispatchQueue.main.async {
            Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.removeFromParent()
    }
    func logMessage(messageText: String) {
        print("message:",messageText)
        messageLbl.text = messageText
    }
    
    func startPreview() {
        if PlatformUtils.isSimulator {
            return
        }
        
        let frontCamera = CameraSource.captureDevice(position: .front)
        let backCamera = CameraSource.captureDevice(position: .back)
        
        if (frontCamera != nil || backCamera != nil) {
            
            let options = CameraSourceOptions { (builder) in
                // To support building with Xcode 10.x.
                #if XCODE_1100
                if #available(iOS 13.0, *) {
                    // Track UIWindowScene events for the key window's scene.
                    // The example app disables multi-window support in the .plist (see UIApplicationSceneManifestKey).
                    builder.orientationTracker = UserInterfaceTracker(scene: UIApplication.shared.keyWindow!.windowScene!)
                }
                #endif
            }
            // Preview our local camera track in the local video preview view.
            camera = CameraSource(options: options, delegate: self)
            localVideoTrack = LocalVideoTrack(source: camera!, enabled: true, name: "Camera")
            
            // Add renderer to video track for local preview
            localVideoTrack!.addRenderer(self.previewView)
            logMessage(messageText: "Video track created")
            
            if (frontCamera != nil && backCamera != nil) {
                // We will flip camera on tap.
                let tap = UITapGestureRecognizer(target: self, action: #selector(VideoCallVC.flipCamera))
                self.previewView.addGestureRecognizer(tap)
            }
            
            camera!.startCapture(device: frontCamera != nil ? frontCamera! : backCamera!) { (captureDevice, videoFormat, error) in
                if let error = error {
                    self.logMessage(messageText: "Capture failed with error.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                    print("Capture failed with error.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")

                } else {
                    self.previewView.shouldMirror = (captureDevice.position == .front)
                }
            }
        }
        else {
            print("No front or back capture device found!")
            self.logMessage(messageText:"No front or back capture device found!")
        }
    }
    @objc func flipCamera() {
        var newDevice: AVCaptureDevice?
        
        if let camera = self.camera, let captureDevice = camera.device {
            if captureDevice.position == .front {
                newDevice = CameraSource.captureDevice(position: .back)
            } else {
                newDevice = CameraSource.captureDevice(position: .front)
            }
            
            if let newDevice = newDevice {
                camera.selectCaptureDevice(newDevice) { (captureDevice, videoFormat, error) in
                    if let error = error {
                        self.logMessage(messageText: "Error selecting capture device.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                        
                    } else {
                        self.previewView.shouldMirror = (captureDevice.position == .front)
                    }
                }
            }
        }
    }
    func setExpandVideoImageViewGesture(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapVideoExpandImage))
        expandVideoViewImage.isUserInteractionEnabled = true
        expandVideoViewImage.addGestureRecognizer(tap)
    }
    
    @objc func tapVideoExpandImage(){
        print("Tapped")
        if let view = self.remoteView{
            self.expandVideoView.insertSubview(view, at: 0)
        }
        expandVideoView.isHidden = false
    }
    
    
    @IBAction func disconnect(sender: AnyObject) {
        
        self.room!.disconnect()
        logMessage(messageText: "Attempting to disconnect from room \(room!.name)")
    }
    
    @IBAction func expandMapViewBtnActn(_ sender: Any) {
        expandMapView.isHidden = true
        timerView.isHidden = false
        isExpandMap = false
    }
    
    @IBAction func expandVideoViewBtnActn(_ sender: Any) {
        self.smallVideoView.insertSubview(self.remoteView!, at: 0)
        expandVideoView.isHidden = true

    }
    @IBAction func muteBtnActn(_ sender: Any) {
        if (self.localAudioTrack != nil) {
            self.localAudioTrack?.isEnabled = !(self.localAudioTrack?.isEnabled)!
            
            // Update the button title
            if (self.localAudioTrack?.isEnabled == true) {
                self.muteUnmuteBtn.setImage(#imageLiteral(resourceName: "audioOff"), for: .normal)
            } else {
                self.muteUnmuteBtn.setImage(#imageLiteral(resourceName: "audioOn"), for: .normal)

            }
        }
    }
    @IBAction func hangUpBtnActn(_ sender: Any) {
//        if let channel = self.generalChannel{
//          channel.leave { (result) in
//            print(result)
//        }
//         }
        if let room = self.room{
           room.disconnect()
        }
       
        //self.navigationController?.popViewController(animated: true)
        self.performSegue(withIdentifier: "showHome", sender: self)
        
    }
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        var text=""
        switch UIDevice.current.orientation{
        case .portrait:
            text="Portrait"
            if isExpandMap{
                self.timerView.isHidden = true
            }
            if let view = self.remoteView{
           self.smallVideoView.insertSubview(view, at: 0)
                
            }
            videoCloseBtn.isHidden = false

            expandVideoView.isHidden = true
            roundView.isHidden = true
        case .portraitUpsideDown:
            text="PortraitUpsideDown"
            
            self.smallVideoView.insertSubview(self.remoteView!, at: 0)
            videoCloseBtn.isHidden = false
            expandVideoView.isHidden = true
            roundView.isHidden = true
        case .landscapeLeft:
            timerView.isHidden = false
            if let view = self.remoteView{
            self.expandVideoView.insertSubview(view, at: 0)
            }
           
            videoCloseBtn.isHidden = true
            expandVideoView.isHidden = false
            roundView.isHidden = false
            
            text="LandscapeLeft"
        case .landscapeRight:
            timerView.isHidden = false
            text="LandscapeRight"
            if let view = self.remoteView{
                self.expandVideoView.insertSubview(view, at: 0)
            }
            videoCloseBtn.isHidden = true
            expandVideoView.isHidden = false
            roundView.isHidden = false
        default:
            text="Another"
        }
        NSLog("You have moved: \(text)")
    }
    
        func convertToDictionary(text: String) -> [String: Any]? {
            if let data = text.data(using: .utf8) {
                do {
                    return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                } catch {
                    print(error.localizedDescription)
                }
            }
            return nil
    }
    
    @objc func updateTime(){
        seconds += 1
        if seconds == 60{
            seconds = 0
            minutes += 1
        }
        if minutes == 60{
             self.timeLbl.text = String(format:"%02i:%02i:%02i",Hours,minutes,seconds)
        }else{
             self.timeLbl.text = String(format:"%02i:%02i",minutes,seconds)
        }
       
    }
    @objc func setTimeForConnecting(){
        if self.remoteView == nil{
            KRProgressHUD.dismiss()
        let alert = UIAlertController(title: "Alert!", message: "Device is not available at this time.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            self.performSegue(withIdentifier: "showHome", sender: self)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    }
    
}
    
// MARK:- VideoViewDelegate
extension VideoCallVC : VideoViewDelegate {
    func videoViewDimensionsDidChange(view: VideoView, dimensions: CMVideoDimensions) {
        self.view.setNeedsLayout()
    }
}

// MARK:- CameraSourceDelegate
extension VideoCallVC : CameraSourceDelegate {
    func cameraSourceDidFail(source: CameraSource, error: Error) {
        logMessage(messageText: "Camera source failed with error: \(error.localizedDescription)")
        print(error)
    }
}
