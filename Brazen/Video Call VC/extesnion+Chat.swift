////
////  extesnion+Chat.swift
////  Brazen
////
////  Created by Sanjeev Mehta on 2/17/20.
////  Copyright © 2020 sanjeevMehta. All rights reserved.
////
//
//import Foundation
//import UIKit
//import TwilioChatClient
//
//extension VideoCallVC {
//
//    func connectClientWithCompletion(completion: @escaping (Bool, NSError?) -> Void) {
//        if (client != nil) {
//            //logout()
//            client = nil
//        }
//
//        requestTokenWithCompletion { succeeded, token in
//            if let token = token, succeeded {
//                print(token)
//                self.initializeClientWithToken(token: token)
//                completion(succeeded, nil)
//            }
//            else {
//                let error = self.errorWithDescription(description: "Could not get access token", code:301)
//                completion(succeeded, error)
//            }
//        }
//    }
//
//    func requestTokenWithCompletion(completion:@escaping (Bool, String?) -> Void) {
//        if let device = UIDevice.current.identifierForVendor?.uuidString {
//            TokenRequestHandler.fetchToken(params: ["device": device, "identity":device]) {response,error in
//                var token: String?
//                token = response["token"] as? String
//                completion(token != nil, token)
//            }
//        }
//    }
//
//    func errorWithDescription(description: String, code: Int) -> NSError {
//        let userInfo = [NSLocalizedDescriptionKey : description]
//        return NSError(domain: "app", code: code, userInfo: userInfo)
//    }
//
//    func initializeClientWithToken(token: String) {
//        DispatchQueue.main.async {
//            UIApplication.shared.isNetworkActivityIndicatorVisible = true
//        }
//        TwilioChatClient.chatClient(withToken: token, properties: nil, delegate: self) { [weak self] result, chatClient in
//            //   guard (result.isSuccessful()) else { return }
//
//            UIApplication.shared.isNetworkActivityIndicatorVisible = true
//            //self?.connected = true
//            self?.client = chatClient
//        }
//    }
//
//    func joinGeneralChatRoomWithUniqueName(name: String?, completion: @escaping (Bool) -> Void) {
//        generalChannel.join { result in
//            if ((result.isSuccessful()) && name != nil) {
//    self.setGeneralChatRoomUniqueNameWithCompletion(completion: completion)
//                return
//            }
//            completion((result.isSuccessful()))
//        }
//
//    }
//
//
//    func joinGeneralChatRoomWithCompletion(completion: @escaping (Bool) -> Void) {
//
//        let uniqueName = self.joinerid
//        if let channelsList = self.channelsList {
//
//         channelsList.channel(withSidOrUniqueName: uniqueName) { result, channel in
//                self.generalChannel = channel
//
//                if self.generalChannel != nil {
//                  print(self.generalChannel.status.rawValue)
//                    if self.generalChannel.status.rawValue == 1{
////                        self.generalChannel.leave(completion: { (result) in
////                            print(result)
////                        })
//                    }else{
//            self.joinGeneralChatRoomWithUniqueName(name: nil, completion: completion)
//                    }
//
//                } else {
//                    print("else")
//
//                    }
//            }
//        }else{
//            completion(false)
//        }
//    }
//
//    func setGeneralChatRoomUniqueNameWithCompletion(completion:@escaping (Bool) -> Void) {
//        generalChannel.setUniqueName(VideoCallVC.defaultChannelUniqueName) { result in
//            completion((result.isSuccessful()))
//        }
//    }
//
//    func createGeneralChatRoomWithCompletion(completion: @escaping (Bool) -> Void) {
//        let channelName = self.joinerid
//        let options = [
//            TCHChannelOptionFriendlyName: channelName,
//            TCHChannelOptionType: TCHChannelType.public.rawValue
//            ] as [String : Any]
//        channelsList!.createChannel(options: options) { result, channel in
//            if (result.isSuccessful()) {
//                self.generalChannel = channel
//            }
//            completion((result.isSuccessful()))
//        }
//    }
//    func populateChannels() {
//        channels = NSMutableOrderedSet()
//
//        channelsList?.userChannelDescriptors { result, paginator in
//            if let paginator = paginator{
//                self.channels?.addObjects(from: paginator.items())
//            }
//           // self.channels?.addObjects(from: paginator!.items())
//            self.sortChannels()
//        }
//
//        channelsList?.publicChannelDescriptors { result, paginator in
//            if let paginator = paginator{
//                self.channels?.addObjects(from: paginator.items())
//            }
//
//            self.sortChannels()
//        }
//    }
//
//    func sortChannels() {
//        let sortSelector = #selector(NSString.localizedCaseInsensitiveCompare(_:))
//        let descriptor = NSSortDescriptor(key: "friendlyName", ascending: true, selector: sortSelector)
//        channels!.sort(using: [descriptor])
//    }
//
//}
//
//class TokenRequestHandler {
//
//    class func postDataFrom(params:[String:String]) -> String {
//        var data = ""
//
//        for (key, value) in params {
//            if let encodedKey = key.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed),
//                let encodedValue = value.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
//                if !data.isEmpty {
//                    data = data + "&"
//                }
//                data += encodedKey + "=" + encodedValue;
//            }
//        }
//
//        return data
//    }
//
//    class func fetchToken(params:[String:String], completion:@escaping (NSDictionary, NSError?) -> Void) {
//
//            if let tokenRequestUrl = URL(string: "https://straw-walrus-2048.twil.io/chat_token") {
//
//            var request = URLRequest(url: tokenRequestUrl)
//            request.httpMethod = "POST"
//            let postString = self.postDataFrom(params: params)
//            request.httpBody = postString.data(using: .utf8)
//            let task = URLSession.shared.dataTask(with: request) { data, response, error in
//                guard let data = data, error == nil else {
//                    print("error=\(String(describing: error))")
//                    completion(NSDictionary(), error as NSError?)
//                    return
//                }
//
//                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
//                    completion(NSDictionary(), NSError(domain: "TWILIO", code: 1000, userInfo: [NSLocalizedDescriptionKey: "Incorrect return code for token request."]))
//                    return
//                }
//
//                do {
//                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
//                    print("json = \(json)")
//                    completion(json as NSDictionary, error as NSError?)
//                } catch let error as NSError {
//                    completion(NSDictionary(), error)
//                }
//            }
//            task.resume()
//        }
//        else {
//            let userInfo = [NSLocalizedDescriptionKey : "TokenRequestUrl Key is missing"]
//            let error = NSError(domain: "app", code: 404, userInfo: userInfo)
//
//            completion(NSDictionary(), error)
//        }
//    }
//}
