//
//  extension+RoomDelegates.swift
//  Brazen
//
//  Created by Sanjeev Mehta on 2/13/20.
//  Copyright © 2020 sanjeevMehta. All rights reserved.
//

import Foundation
import TwilioVideo
import KRProgressHUD

//Twilio Video Call Room Delegates Methods//////
extension VideoCallVC : RoomDelegate {
    
    func roomDidConnect(room: Room) {
        logMessage(messageText: "Connected to room \(room.name) as \(room.localParticipant?.identity ?? "")")
        print("Connected")
        self.connectDisconnectBtn.setTitle("Disconnect", for: .normal)
        // This example only renders 1 RemoteVideoTrack at a time. Listen for all events to decide which track to render.
        for remoteParticipant in room.remoteParticipants {
            remoteParticipant.delegate = self
        }
       
        
    }
    
    func roomDidDisconnect(room: Room, error: Error?) {
        KRProgressHUD.dismiss()
        logMessage(messageText: "Disconnected from room \(room.name), error = \(String(describing: error))")
        
        self.cleanupRemoteParticipant()
        self.room = nil
        
       // self.showRoomUI(inRoom: false)
    }
    
    func roomDidFailToConnect(room: Room, error: Error) {
        logMessage(messageText: "Failed to connect to room with error = \(String(describing: error))")
        self.room = nil
        
       // self.showRoomUI(inRoom: false)
    }
    
    func roomIsReconnecting(room: Room, error: Error) {
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
            KRProgressHUD.showOn(self).show(withMessage: "Reconnecting...")
            logMessage(messageText: "Reconnecting to room \(room.name), error = \(String(describing: error))")
        }else{
            let alert = UIAlertController(title: "Alert", message: "Something went wrong please check your internet connection.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                self.performSegue(withIdentifier: "showHome", sender: self)
            }))
            self.present(alert, animated: true, completion: nil)
        }
      
       
    }
    
    func roomDidReconnect(room: Room) {
        logMessage(messageText: "Reconnected to room \(room.name)")
        KRProgressHUD.dismiss()
    }
    
    func participantDidConnect(room: Room, participant: RemoteParticipant) {
        // Listen for events from all Participants to decide which RemoteVideoTrack to render.
        participant.delegate = self
        
        logMessage(messageText: "Participant \(participant.identity) connected with \(participant.remoteAudioTracks.count) audio and \(participant.remoteVideoTracks.count) video tracks")
        
    }
    
    func participantDidDisconnect(room: Room, participant: RemoteParticipant) {
        KRProgressHUD.dismiss()
//        self.generalChannel.leave { (result) in
//            print(result)
//        }
    logMessage(messageText: "Room \(room.name), Participant \(participant.identity) disconnected")
        let alert = UIAlertController(title: "Alert", message: "Participant is disconnected", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
             self.performSegue(withIdentifier: "showHome", sender: self)
        }))
       self.present(alert, animated: true, completion: nil)
       
        
        // Nothing to do in this example. Subscription events are used to add/remove renderers.
    }
}


extension VideoCallVC: RemoteParticipantDelegate{
    
    func remoteParticipantDidPublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
    // Remote Participant has offered to share the video Track.
    
    logMessage(messageText: "Participant \(participant.identity) published \(publication.trackName) video track")
    }
    
    func remoteParticipantDidUnpublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
    // Remote Participant has stopped sharing the video Track.
    
    logMessage(messageText: "Participant \(participant.identity) unpublished \(publication.trackName) video track")
    }
    
    func remoteParticipantDidPublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
    // Remote Participant has offered to share the audio Track.
    
    logMessage(messageText: "Participant \(participant.identity) published \(publication.trackName) audio track")
    }
    
    func remoteParticipantDidUnpublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
    // Remote Participant has stopped sharing the audio Track.
    
    logMessage(messageText: "Participant \(participant.identity) unpublished \(publication.trackName) audio track")
    }
    
    func didSubscribeToVideoTrack(videoTrack: RemoteVideoTrack, publication: RemoteVideoTrackPublication, participant: RemoteParticipant) {
    // The LocalParticipant is subscribed to the RemoteParticipant's video Track. Frames will begin to arrive now.
    
    logMessage(messageText: "Subscribed to \(publication.trackName) video track for Participant \(participant.identity)")
    
    if (self.remoteParticipant == nil) {
    _ = renderRemoteParticipant(participant: participant)
    }
    }
    
    func didUnsubscribeFromVideoTrack(videoTrack: RemoteVideoTrack, publication: RemoteVideoTrackPublication, participant: RemoteParticipant) {
    // We are unsubscribed from the remote Participant's video Track. We will no longer receive the
    // remote Participant's video.
    
    logMessage(messageText: "Unsubscribed from \(publication.trackName) video track for Participant \(participant.identity)")
    
    if self.remoteParticipant == participant {
    cleanupRemoteParticipant()
    
    // Find another Participant video to render, if possible.
    if var remainingParticipants = room?.remoteParticipants,
    let index = remainingParticipants.index(of: participant) {
    remainingParticipants.remove(at: index)
    renderRemoteParticipants(participants: remainingParticipants)
    }
    }
    }
    
    func didSubscribeToAudioTrack(audioTrack: RemoteAudioTrack, publication: RemoteAudioTrackPublication, participant: RemoteParticipant) {
    // We are subscribed to the remote Participant's audio Track. We will start receiving the
    // remote Participant's audio now.
    
    logMessage(messageText: "Subscribed to \(publication.trackName) audio track for Participant \(participant.identity)")
    }
    
    func didUnsubscribeFromAudioTrack(audioTrack: RemoteAudioTrack, publication: RemoteAudioTrackPublication, participant: RemoteParticipant) {
    // We are unsubscribed from the remote Participant's audio Track. We will no longer receive the
    // remote Participant's audio.
    
    logMessage(messageText: "Unsubscribed from \(publication.trackName) audio track for Participant \(participant.identity)")
    }
    
    func remoteParticipantDidEnableVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
    logMessage(messageText: "Participant \(participant.identity) enabled \(publication.trackName) video track")
    }
    
    func remoteParticipantDidDisableVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
    logMessage(messageText: "Participant \(participant.identity) disabled \(publication.trackName) video track")
    }
    
    func remoteParticipantDidEnableAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
    logMessage(messageText: "Participant \(participant.identity) enabled \(publication.trackName) audio track")
    }
    
    func remoteParticipantDidDisableAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
    logMessage(messageText: "Participant \(participant.identity) disabled \(publication.trackName) audio track")
        
    }
    
    func didFailToSubscribeToAudioTrack(publication: RemoteAudioTrackPublication, error: Error, participant: RemoteParticipant) {
    logMessage(messageText: "FailedToSubscribe \(publication.trackName) audio track, error = \(String(describing: error))")
         print(error)
    }
    
    func didFailToSubscribeToVideoTrack(publication: RemoteVideoTrackPublication, error: Error, participant: RemoteParticipant) {
    logMessage(messageText: "FailedToSubscribe \(publication.trackName) video track, error = \(String(describing: error))")
        print(error)
    }
    
    
    func renderRemoteParticipant(participant : RemoteParticipant) -> Bool {
        // This example renders the first subscribed RemoteVideoTrack from the RemoteParticipant.
        let videoPublications = participant.remoteVideoTracks
        for publication in videoPublications {
            if let subscribedVideoTrack = publication.remoteTrack,
                publication.isTrackSubscribed {
                setupRemoteVideoView()
                subscribedVideoTrack.addRenderer(self.remoteView!)
                self.remoteParticipant = participant
                return true
            }
        }
        return false
    }
    func renderRemoteParticipants(participants : Array<RemoteParticipant>) {
        for participant in participants {
            // Find the first renderable track.
            if participant.remoteVideoTracks.count > 0,
                renderRemoteParticipant(participant: participant) {
                break
            }
        }
    }
    
    func cleanupRemoteParticipant() {
        if self.remoteParticipant != nil {
            self.remoteView?.removeFromSuperview()
            self.remoteView = nil
            self.remoteParticipant = nil
        }
    }
}

