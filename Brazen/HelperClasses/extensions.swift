//
//  extensions.swift
//  Brazen
//
//  Created by Sanjeev Mehta on 2/14/20.
//  Copyright © 2020 sanjeevMehta. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    func dropShadow(cornerRadius:Bool) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.7
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 3
        layer.shouldRasterize = true
        
        if cornerRadius{
            layer.cornerRadius = 10
        }
    }

}
extension UIViewController{
    func showAlert(message:String){
        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
   
    
}
extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}
