//
//  VideoView.swift
//  Brazen
//
//  Created by Sanjeev Mehta on 2/14/20.
//  Copyright © 2020 sanjeevMehta. All rights reserved.
//

import Foundation
import AVFoundation
import AVKit

class VideosView: UIView {
    
    var playerLayer: AVPlayerLayer?
    var player: AVPlayer?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func configure(forResource: String,gravity:AVLayerVideoGravity) {
        guard let path = Bundle.main.path(forResource: forResource, ofType:"mp4") else {
            debugPrint("video.m4v not found")
            return
        }
        let videoURL = URL(fileURLWithPath: path)
        player = AVPlayer(url: videoURL)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.frame = bounds
        playerLayer?.videoGravity = gravity
        if let playerLayer = self.playerLayer {
            layer.addSublayer(playerLayer)
        }
        
    }
    
    func play() {
        
        if player?.timeControlStatus != AVPlayer.TimeControlStatus.playing
        {
            player?.play()
        }
    }
    
    func pause() {
        player?.pause()
    }
    
    func stop() {
        player?.pause()
        player?.seek(to: CMTime.zero)
    }
    
    
    
}
