//
//  WebSerivces.swift
//  Brazen
//
//  Created by Sanjeev Mehta on 2/13/20.
//  Copyright © 2020 sanjeevMehta. All rights reserved.
//

import Foundation
import Alamofire
import KRProgressHUD
import SwiftyJSON

class WebService:NSObject{
    
    static let shareInstance = WebService()
    func getToken(view:UIViewController,parm:[String:Any],completion: @escaping(JSON,Error?) -> ()){
      
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
            let alert = UIAlertController(title: "Alert", message: "Something went wrong. please check your internet connection.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                KRProgressHUD.dismiss()
                view.performSegue(withIdentifier: "showHome", sender: self)
            }))
            view.present(alert, animated: true, completion: nil)
            return
        }
        let urlString = "http://etitbit.com/twilio/create-room.php"
        guard let url = URL(string: urlString) else{return}
        
        Alamofire.request(url,method: .post, parameters: parm,encoding: URLEncoding.queryString).validate(statusCode: 200..<500).responseJSON {response in
            switch response.result{
            case .success(let value):
                //KRProgressHUD.dismiss()
                do {
                    let jsonData = try JSON(data: response.data!)
                    completion(jsonData, nil)
                    
                }catch{
                }
            case .failure(let err):
                print(err)
                
                do {
                    try completion(JSON(data: NSData() as Data), err)
                }catch{
                }
            }
        }
    }
}
